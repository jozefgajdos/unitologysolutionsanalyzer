﻿using System;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestHelper;
using UnitologySolutionsAnalyzer.Usage;
using System.Text.RegularExpressions;

namespace UnitologySolutionsAnalyzer.Test.Usage
{
    [TestClass]
    public class RegexMatchTest : CodeFixVerifier
    {
        [TestMethod]
        public void VerifySuccessSourse()
        {
            string test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        public class TypeName
        {  
          public TypeName()
          {
          }

          public void Test()
          {
            Match match = Regex.Match("""", ""[A-Za-z0-9](abc|cd)?"");
            Console.WriteLine(match.Value);
          } 
        }
    }";
            this.VerifyCSharpDiagnostic(test);
        }

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer()
        {
            return new RegexMatchRule();
        }
    }
}
