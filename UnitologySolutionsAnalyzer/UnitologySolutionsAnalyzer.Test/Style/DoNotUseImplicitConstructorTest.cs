﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestHelper;
using UnitologySolutionsAnalyzer;
using UnitologySolutionsAnalyzer.Style;

namespace UnitologySolutionsAnalyzer.Test.Style
{
    [TestClass]
    public class DoNotUseImplicitConstructorTest : CodeFixVerifier
    {
        [TestMethod]
        public void VerifySuccessSourse()
        {
            string test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        public class TypeName
        {  
          private int foo = 15;
          
          public TypeName()
          {
          }

          public void Test()
          {
            Console.WriteLine(""test {0}"", this.foo);
          } 
        }
    }";
            this.VerifyCSharpDiagnostic(test);
            this.VerifyCSharpDiagnostic(test.Replace("public TypeName()", "private TypeName()"));
            this.VerifyCSharpDiagnostic(test.Replace("public TypeName()", "protected TypeName()"));
        }

        [TestMethod]
        public void VerifyProgramSuccessSourse()
        {
            string test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        public class Program
        {  
           static void Main(string[] args)
           {
           }
        }      
    }";
            this.VerifyCSharpDiagnostic(test);
        }

        [TestMethod]
        public void VerifyStaticClass()
        {
            string test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        public static class TypeName
        {  
          private int foo = 15;
          
          public void Test()
          {
            Console.WriteLine(""test {0}"", this.foo);
          } 
        }
    }";
            this.VerifyCSharpDiagnostic(test);
        }

        [TestMethod]
        public void VerifyMissingPublicConstructorDiagnose()
        {
            string test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        public class TypeName
        {  
          private int foo = 15;
          
          public void Test()
          {
            Console.WriteLine(""test {0}"", this.foo);
          } 
        }
    }";

            DiagnosticResult expected = new DiagnosticResult()
            {
                Id = DoNotUseImplicitConstructorRule.DiagnosticId,
                Message = String.Format("Class '{0}' not contains an explicit constructor.", "TypeName"),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 11, 9)
                        }
            };

            VerifyCSharpDiagnostic(test, expected);
        }


        //protected override CodeFixProvider GetCSharpCodeFixProvider()
        //{
        //    return new MissingPublicConstructorRule();
        //}

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer()
        {
            return new DoNotUseImplicitConstructorRule();
        }
    }
}
