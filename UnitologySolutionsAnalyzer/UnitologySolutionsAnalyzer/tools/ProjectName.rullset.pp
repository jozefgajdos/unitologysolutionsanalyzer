﻿<?xml version="1.0" encoding="utf-8"?>
<RuleSet Name="Rules for $rootnamespace$" Description="Code analysis rules for $rootnamespace$.csproj." ToolsVersion="14.0">
  <Rules AnalyzerId="Microsoft.CodeAnalysis.CSharp.Features" RuleNamespace="Microsoft.CodeAnalysis.CSharp.Features">
    <Rule Id="IDE0001" Action="Warning" />
    <Rule Id="IDE0002" Action="Warning" />
    <Rule Id="IDE0003" Action="None" />
  </Rules>
</RuleSet>
