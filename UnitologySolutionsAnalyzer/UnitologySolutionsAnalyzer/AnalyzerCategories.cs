﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitologySolutionsAnalyzer
{
    /// <summary>
    /// Analyzer categories names.
    /// </summary>
    internal static class AnalyzerCategories
    {
        /// <summary>
        /// The naming category name.
        /// </summary>
        public const string Naming = "Naming";

        /// <summary>
        /// The style category name.
        /// </summary>
        public const string Style = "Style";

        /// <summary>
        /// The usage category name.
        /// </summary>
        public const string Usage = "Usage";
    }
}
