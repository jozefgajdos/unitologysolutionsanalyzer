﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using System;
using System.Linq;
using System.Collections.Immutable;

namespace UnitologySolutionsAnalyzer.Usage
{
    /// <summary>
    /// The regular expression is invalid and will fail at run-time.
    /// </summary>
    /// <remarks>
    /// From https://github.com/code-cracker/code-cracker/blob/master/src/CSharp/CodeCracker/Usage/RegexAnalyzer.cs
    /// </remarks>
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class RegexMatchRule : DiagnosticAnalyzer
    {
        /// <summary>
        /// The diagnostic identifier &quot;BadRegexString&quot;.
        /// </summary>
        public const string DiagnosticId = "BadRegexString";

        internal static readonly DiagnosticDescriptor Rule = new DiagnosticDescriptor(
            DiagnosticId,
            "Your Regex expression is wrong",
            "{0}",
            AnalyzerCategories.Usage,
            DiagnosticSeverity.Error,
            isEnabledByDefault: true,
            description: "This diagnostic compile the Regex expression and trigger if the compilation fail by throwing an exception.");

        /// <summary>
        /// Gets the supported diagnostics.
        /// </summary>
        /// <value>
        /// The supported diagnostics.
        /// </value>
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
        {
            get
            {
                return ImmutableArray.Create(Rule);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexMatchRule"/> class.
        /// </summary>
        public RegexMatchRule()
        {

        }

        /// <summary>
        /// Initializes the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeAction(Analyzer, SyntaxKind.InvocationExpression);
        }

        /// <summary>
        /// Analyzers the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        private static void Analyzer(SyntaxNodeAnalysisContext context)
        {
            InvocationExpressionSyntax invocationExpression = (InvocationExpressionSyntax)context.Node;

            MemberAccessExpressionSyntax memberExpresion = invocationExpression.Expression as MemberAccessExpressionSyntax;
            string identifier = memberExpresion?.Name?.Identifier.ValueText;

            switch (identifier)
            {
                case "Match":
                    CheckRegex(context, memberExpresion, "Match", t => System.Text.RegularExpressions.Regex.Match("", t));
                   // CheckMatch(context, memberExpresion);
                    break;
                case "Matches":
                    CheckRegex(context, memberExpresion, "Matches", t => System.Text.RegularExpressions.Regex.Matches("", t));
                    //CheckMatches(context, memberExpresion);
                    break;
                case "IsMatch":
                    CheckRegex(context, memberExpresion, "IsMatch", t => System.Text.RegularExpressions.Regex.IsMatch("", t));
                    //CheckIsMatch(context, memberExpresion);
                    break;
                case "Replace":
                    CheckRegex(context, memberExpresion, "Replace", t => System.Text.RegularExpressions.Regex.Replace("", t, ""), 3);
                    //CheckReplace(context, memberExpresion);
                    break;
                case "Split":
                    CheckRegex(context, memberExpresion, "Split", t => System.Text.RegularExpressions.Regex.Split("", t));
                    // CheckSplit(context, memberExpresion);
                    break;
            }
        }

        private static void CheckRegex(SyntaxNodeAnalysisContext context, MemberAccessExpressionSyntax memberExpresion, string methodName, Action<string> testRefex, int argumentsMinCount = 2)
        {
            string methodPrefix = string.Concat("System.Text.RegularExpressions.", methodName, ".Match(");
            if (MemberSymbolStartWith(context, memberExpresion, methodPrefix))
            {
                InvocationExpressionSyntax invocationExpression = (InvocationExpressionSyntax)context.Node;
                ArgumentListSyntax argumentList = invocationExpression.ArgumentList as ArgumentListSyntax;
                if ((argumentList?.Arguments.Count ?? 0) != argumentsMinCount)
                {
                    return;
                }

                LiteralExpressionSyntax regexLiteral = argumentList.Arguments[1].Expression as LiteralExpressionSyntax;
                if (regexLiteral == null)
                {
                    return;
                }

                Optional<object> regexOpt = context.SemanticModel.GetConstantValue(regexLiteral);
                string regex = regexOpt.Value as string;

                try
                {
                    testRefex.Invoke(regex);
                }
                catch (ArgumentException e)
                {
                    Diagnostic diag = Diagnostic.Create(Rule, regexLiteral.GetLocation(), e.Message);
                    context.ReportDiagnostic(diag);
                }
            }

        }

        private static bool MemberSymbolStartWith(SyntaxNodeAnalysisContext context, MemberAccessExpressionSyntax memberExpresion, string value)
        {
            IMethodSymbol memberSymbol = context.SemanticModel.GetSymbolInfo(memberExpresion).Symbol as IMethodSymbol;
            if (memberSymbol == null)
            {
                return false;
            }
            string orgValue = memberSymbol.ToString();

            return orgValue.StartsWith(value, StringComparison.Ordinal);
        }

        //private static void CheckMatch(SyntaxNodeAnalysisContext context, MemberAccessExpressionSyntax memberExpresion)
        //{
        //    if (MemberSymbolStartWith(context, memberExpresion, "System.Text.RegularExpressions.Regex.Match("))
        //    {
        //        InvocationExpressionSyntax invocationExpression = (InvocationExpressionSyntax)context.Node;
        //        ArgumentListSyntax argumentList = invocationExpression.ArgumentList as ArgumentListSyntax;
        //        if ((argumentList?.Arguments.Count ?? 0) != 2)
        //        {
        //            return;
        //        }

        //        LiteralExpressionSyntax regexLiteral = argumentList.Arguments[1].Expression as LiteralExpressionSyntax;
        //        if (regexLiteral == null)
        //        {
        //            return;
        //        }

        //        Optional<object> regexOpt = context.SemanticModel.GetConstantValue(regexLiteral);
        //        string regex = regexOpt.Value as string;

        //        try
        //        {
        //            System.Text.RegularExpressions.Regex.Match(string.Empty, regex);
        //        }
        //        catch (ArgumentException e)
        //        {
        //            Diagnostic diag = Diagnostic.Create(Rule, regexLiteral.GetLocation(), e.Message);
        //            context.ReportDiagnostic(diag);
        //        }
        //    }
        //    else
        //    {
        //        return;
        //    }
        //}

        //private static void CheckMatches(SyntaxNodeAnalysisContext context, MemberAccessExpressionSyntax memberExpresion)
        //{
        //    if (MemberSymbolStartWith(context, memberExpresion, "System.Text.RegularExpressions.Regex.Matches("))
        //    {
        //        InvocationExpressionSyntax invocationExpression = (InvocationExpressionSyntax)context.Node;
        //        ArgumentListSyntax argumentList = invocationExpression.ArgumentList as ArgumentListSyntax;
        //        if ((argumentList?.Arguments.Count ?? 0) < 2)
        //        {
        //            return;
        //        }

        //        LiteralExpressionSyntax regexLiteral = argumentList.Arguments[1].Expression as LiteralExpressionSyntax;
        //        if (regexLiteral == null)
        //        {
        //            return;
        //        }

        //        Optional<object> regexOpt = context.SemanticModel.GetConstantValue(regexLiteral);
        //        string regex = regexOpt.Value as string;

        //        try
        //        {
        //            System.Text.RegularExpressions.Regex.Matches(string.Empty, regex);
        //        }
        //        catch (ArgumentException e)
        //        {
        //            Diagnostic diag = Diagnostic.Create(Rule, regexLiteral.GetLocation(), e.Message);
        //            context.ReportDiagnostic(diag);
        //        }
        //    }
        //}

        //private static void CheckIsMatch(SyntaxNodeAnalysisContext context, MemberAccessExpressionSyntax memberExpresion)
        //{
        //    if (MemberSymbolStartWith(context, memberExpresion, "System.Text.RegularExpressions.Regex.IsMatch("))
        //    {
        //        InvocationExpressionSyntax invocationExpression = (InvocationExpressionSyntax)context.Node;
        //        ArgumentListSyntax argumentList = invocationExpression.ArgumentList as ArgumentListSyntax;
        //        if ((argumentList?.Arguments.Count ?? 0) < 2)
        //        {
        //            return;
        //        }

        //        LiteralExpressionSyntax regexLiteral = argumentList.Arguments[1].Expression as LiteralExpressionSyntax;
        //        if (regexLiteral == null)
        //        {
        //            return;
        //        }

        //        Optional<object> regexOpt = context.SemanticModel.GetConstantValue(regexLiteral);
        //        string regex = regexOpt.Value as string;

        //        try
        //        {
        //            System.Text.RegularExpressions.Regex.IsMatch(string.Empty, regex);
        //        }
        //        catch (ArgumentException e)
        //        {
        //            Diagnostic diag = Diagnostic.Create(Rule, regexLiteral.GetLocation(), e.Message);
        //            context.ReportDiagnostic(diag);
        //        }
        //    }
        //}

        //private static void CheckReplace(SyntaxNodeAnalysisContext context, MemberAccessExpressionSyntax memberExpresion)
        //{
        //    if (MemberSymbolStartWith(context, memberExpresion, "System.Text.RegularExpressions.Regex.Replace("))
        //    {
        //        InvocationExpressionSyntax invocationExpression = (InvocationExpressionSyntax)context.Node;
        //        ArgumentListSyntax argumentList = invocationExpression.ArgumentList as ArgumentListSyntax;
        //        if ((argumentList?.Arguments.Count ?? 0) < 3)
        //        {
        //            return;
        //        }

        //        LiteralExpressionSyntax regexLiteral = argumentList.Arguments[1].Expression as LiteralExpressionSyntax;
        //        if (regexLiteral == null)
        //        {
        //            return;
        //        }

        //        Optional<object> regexOpt = context.SemanticModel.GetConstantValue(regexLiteral);
        //        string regex = regexOpt.Value as string;

        //        try
        //        {
        //            System.Text.RegularExpressions.Regex.Replace(string.Empty, regex, string.Empty);
        //        }
        //        catch (ArgumentException e)
        //        {
        //            Diagnostic diag = Diagnostic.Create(Rule, regexLiteral.GetLocation(), e.Message);
        //            context.ReportDiagnostic(diag);
        //        }
        //    }
        //}

        //private static void CheckSplit(SyntaxNodeAnalysisContext context, MemberAccessExpressionSyntax memberExpresion)
        //{
        //    if (MemberSymbolStartWith(context, memberExpresion, "System.Text.RegularExpressions.Regex.Split("))
        //    {
        //        InvocationExpressionSyntax invocationExpression = (InvocationExpressionSyntax)context.Node;
        //        ArgumentListSyntax argumentList = invocationExpression.ArgumentList as ArgumentListSyntax;
        //        if ((argumentList?.Arguments.Count ?? 0) < 2)
        //        {
        //            return;
        //        }

        //        LiteralExpressionSyntax regexLiteral = argumentList.Arguments[1].Expression as LiteralExpressionSyntax;
        //        if (regexLiteral == null)
        //        {
        //            return;
        //        }

        //        Optional<object> regexOpt = context.SemanticModel.GetConstantValue(regexLiteral);
        //        string regex = regexOpt.Value as string;

        //        try
        //        {
        //            System.Text.RegularExpressions.Regex.Split(string.Empty, regex);
        //        }
        //        catch (ArgumentException e)
        //        {
        //            Diagnostic diag = Diagnostic.Create(Rule, regexLiteral.GetLocation(), e.Message);
        //            context.ReportDiagnostic(diag);
        //        }
        //    }
        //}
    }
}
