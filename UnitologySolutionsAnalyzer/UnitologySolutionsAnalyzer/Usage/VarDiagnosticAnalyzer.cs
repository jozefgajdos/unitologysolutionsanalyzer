﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using System;
using System.Linq;
using System.Collections.Immutable;

namespace UnitologySolutionsAnalyzer.Usage
{
    /// <summary>
    /// Analyzer for <c>var</c> keyword.
    /// </summary>
    /// <remarks>
    /// From https://github.com/DotNetAnalyzers/VarUsageAnalyzer/blob/master/src/DotNetDoodle.Analyzers/DotNetDoodle.Analyzers/VarCodeFixProvider.cs
    /// </remarks>
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class VarDiagnosticAnalyzer : DiagnosticAnalyzer
    {
        public const string DiagnosticId = "TU0001";
        internal static DiagnosticDescriptor Rule = new DiagnosticDescriptor(
            id: DiagnosticId,
            title: "Avoid Using Implicit Type",
            messageFormat: "Variable declation has implicit type 'var' instead of explicit type.",
            category: AnalyzerCategories.Usage,
            defaultSeverity: DiagnosticSeverity.Warning,
            isEnabledByDefault: true,
            description: "Variable declation has implicit type 'var' instead of explicit type.");

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
        {
            get
            {
                return ImmutableArray.Create(Rule);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VarDiagnosticAnalyzer"/> class.
        /// </summary>
        public VarDiagnosticAnalyzer()
        {

        }

        /// <summary>
        /// Called once at session start to register actions in the analysis context.
        /// </summary>
        /// <param name="context"></param>
        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.VariableDeclaration);
        }

        // privates

        private void AnalyzeNode(SyntaxNodeAnalysisContext context)
        {
            VariableDeclarationSyntax variableDeclaration = (VariableDeclarationSyntax)context.Node;
            if (IsReportable(variableDeclaration, context.SemanticModel))
            {
                context.ReportDiagnostic(Diagnostic.Create(Rule, variableDeclaration.Type.GetLocation()));
            }
        }

        private bool IsReportable(VariableDeclarationSyntax variableDeclaration, SemanticModel semanticModel)
        {
            
            bool result;
            TypeSyntax variableTypeName = variableDeclaration.Type;
            if (variableTypeName.IsVar)
            {
                // Implicitly-typed variables cannot have multiple declarators. Short circuit if it does.
                bool hasMultipleVariables = variableDeclaration.Variables.Skip(1).Any();
                if (hasMultipleVariables == false)
                {

                    // Special case: Ensure that 'var' isn't actually an alias to another type. (e.g. using var = System.String).
                    IAliasSymbol aliasInfo = semanticModel.GetAliasInfo(variableTypeName);
                    if (aliasInfo == null)
                    {
                        // Retrieve the type inferred for var.
                        ITypeSymbol type = semanticModel.GetTypeInfo(variableTypeName).ConvertedType;

                        // Special case: Ensure that 'var' isn't actually a type named 'var'.
                        if (type.Name.Equals("var", StringComparison.Ordinal) == false)
                        {
                            // Special case: Ensure that the type is not an anonymous type.
                            if (type.IsAnonymousType == false)
                            {
                                result = true;

                                // Special case: Ensure that it's not a 'new' expression.
                                //if (variableDeclaration.Variables.First().Initializer.Value.IsKind(SyntaxKind.ObjectCreationExpression))
                                //{
                                //    result = false;
                                //}
                                //else
                                //{
                                //    result = true;
                                //}
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }

            return result;
        }
    }
}
