﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitologySolutionsAnalyzer.Extensions
{
    /// <summary>
    /// Extensions for <see cref="string"/>.
    /// </summary>
    internal static class StringExtensions
    {
        /// <summary>
        /// Determines whether string is camel case.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns><c>true</c> if string is camel case, otherwrise <c>false</c></returns>
        /// <exception cref="System.ArgumentNullException">value</exception>
        public static bool IsCamelCase(this string value)
        {
            if(value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            for(int i=0;i<value.Length;i++)
            {
                if(!Char.IsLetterOrDigit(value,i))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determines whether string is camel case.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="firstLetterIsUpper">if set to <c>true</c> then first letter must by upper.</param>
        /// <returns>
        ///   <c>true</c> if string is camel case, otherwrise <c>false</c></returns>
        /// <exception cref="System.ArgumentNullException">value</exception>
        /// <exception cref="System.ArgumentException">Argument value is empty string.</exception>
        public static bool IsCamelCase(this string value, bool firstLetterIsUpper)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException("Argument value is empty string.");
            }

            for (int i = 0; i < value.Length; i++)
            {
                if (!Char.IsLetterOrDigit(value, i))
                {
                    return false;
                }
            }

            if(Char.IsUpper(value, 0) != firstLetterIsUpper)
            {
                return false;
            }

            return true;
        }
    }
}
