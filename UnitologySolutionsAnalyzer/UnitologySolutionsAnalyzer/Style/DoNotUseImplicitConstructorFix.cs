﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Rename;
using Microsoft.CodeAnalysis.Text;

namespace UnitologySolutionsAnalyzer.Style
{
    /// <summary>
    /// Fix hint for missing constructor in nonstatic class.
    /// </summary>
    public class DoNotUseImplicitConstructorFix
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DoNotUseImplicitConstructorFix"/> class.
        /// </summary>
        public DoNotUseImplicitConstructorFix()
        {

        }
    }
}
