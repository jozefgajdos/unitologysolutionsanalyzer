﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace UnitologySolutionsAnalyzer.Style
{
    /// <summary>
    /// Missing constructor analyzer rule.
    /// </summary>
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class DoNotUseImplicitConstructorRule : DiagnosticAnalyzer
    {

        /// <summary>
        /// The diagnostic identifier &quot;DoNotUseExplicitConstructorRule&quot;.
        /// </summary>
        public const string DiagnosticId = "DoNotUseExplicitConstructorRule";


        private static DiagnosticDescriptor Rule = new DiagnosticDescriptor(DiagnosticId,
            "Class not contains an explicit constructor.",
            "Class '{0}' not contains an explicit constructor.",
            AnalyzerCategories.Style,
            DiagnosticSeverity.Warning,
            isEnabledByDefault: true,
            description: "Class must contains at least one explicit constructor with documentation.");

        /// <summary>
        /// Gets the supported diagnostics.
        /// </summary>
        /// <value>
        /// The supported diagnostics.
        /// </value>
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
        {
            get
            {
                return ImmutableArray.Create(Rule);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DoNotUseImplicitConstructorRule"/> class.
        /// </summary>
        public DoNotUseImplicitConstructorRule()
        {

        }

        /// <summary>
        /// Called once at session start to register actions in the analysis context.
        /// </summary>
        /// <param name="context">The analysis context.</param>
        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.ClassDeclaration);
        }

        private static void AnalyzeNode(SyntaxNodeAnalysisContext context)
        {
            ClassDeclarationSyntax classDeclaration = (ClassDeclarationSyntax)context.Node;

            if (classDeclaration.Modifiers.Any(t => t.IsKind(SyntaxKind.StaticKeyword)))
            {
                return;
            }

            if (context.Node.ChildNodes().OfType<ConstructorDeclarationSyntax>().Any())
            {
                return;
            }

            if (string.Equals(classDeclaration.Identifier.ValueText, "Program", StringComparison.Ordinal))
            {
                return;
            }

            Diagnostic diagnostic = Diagnostic.Create(Rule, classDeclaration.GetLocation(), classDeclaration.Identifier.ValueText);
            context.ReportDiagnostic(diagnostic);
        }
    }
}
