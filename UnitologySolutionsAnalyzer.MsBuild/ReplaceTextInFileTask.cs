﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Build.Utilities;
using System.Security;
using Microsoft.Build.Framework;
using System.IO;

namespace UnitologySolutionsAnalyzer.MsBuild
{
    /// <summary>
    /// Task to replace text in text files.
    /// </summary>
    public class ReplaceTextInFileTask : Task
    {
        private const int CurrentYear = 2015;

        private Dictionary<string, Func<string, string>> evaluetors;

        /// <summary>
        /// Gets or sets the path to processed file.
        /// </summary>
        /// <value>
        /// Path to processed file.
        /// </value>
        public string File
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the path to processed files.
        /// </summary>
        /// <value>
        /// The path to processed files.
        /// </value>
        public ITaskItem[] Files
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the old text value.
        /// </summary>
        /// <value>
        /// The old text value.
        /// </value>
        [Required]
        public string OldValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the new text value or special evaluator type string.
        /// </summary>
        /// <value>
        /// The new text value or special evaluator type string.
        /// </value>
        [Required]
        public string NewValue
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReplaceTextInFileTask"/> class.
        /// </summary>
        public ReplaceTextInFileTask()
        {
            this.OldValue = null;
            this.NewValue = null;
            this.evaluetors = new Dictionary<string, Func<string, string>>();
            this.InitEvaluators(this.evaluetors);
        }

        /// <summary>
        /// Execute task <see cref="ReplaceTextInFileTask"/> and replace texts.
        /// </summary>
        /// <returns><c>true</c> if execute success, otherwrise <c>false</c>.</returns>
        public override bool Execute()
        {
            if (!this.CheckArguments())
            {
                return false;
            }

            foreach (string path in this.GetAllFiles())
            {
                if (!System.IO.File.Exists(path))
                {
                    this.Log.LogError("ReplaceTextInFileTask: File '{0}' has not found.", path);
                    return false;
                }

                string content = System.IO.File.ReadAllText(path);
                content = this.ReplaceText(content, this.OldValue, this.NewValue);

                FileInfo fileInfo = new FileInfo(path);
                if (fileInfo.Attributes.HasFlag(FileAttributes.ReadOnly))
                {
                    fileInfo.Attributes = FileAttributes.Normal;
                }

                fileInfo.Delete();

                System.IO.File.WriteAllText(path, content);
                this.Log.LogMessage("Replace '{0}' in file '{1}'", this.OldValue, path);
            }

            return true;
        }

        /// <summary>
        /// Gets all files to processing.
        /// </summary>
        /// <returns>Paths to files to processing.</returns>
        protected IEnumerable<string> GetAllFiles()
        {
            if (string.IsNullOrEmpty(this.File))
            {
                return this.Files.Select(t => t.ItemSpec);
            }
            else
            {
                return new string[] { this.File };
            }
        }

        /// <summary>
        /// Replaces the <paramref name="oldText"/> to <paramref name="newText"/> value or spcial sexunce.
        /// </summary>
        /// <param name="originalText">The file original text.</param>
        /// <param name="oldText">The old text value.</param>
        /// <param name="newText">The new text value.</param>
        /// <returns>Orifginal text with replacment.</returns>
        /// <exception cref="System.ArgumentNullException">
        /// originalText
        /// or
        /// oldText
        /// or
        /// newText
        /// </exception>
        protected string ReplaceText(string originalText, string oldText, string newText)
        {
            if (originalText == null)
            {
                throw new ArgumentNullException(nameof(originalText));
            }
            if (oldText == null)
            {
                throw new ArgumentNullException(nameof(oldText));
            }
            if (newText == null)
            {
                throw new ArgumentNullException(nameof(newText));
            }

            Func<string, string> evaluator;
            if(this.evaluetors.TryGetValue(newText, out evaluator))
            {
                newText = evaluator.Invoke(oldText);
            }

            return originalText.Replace(oldText, newText);
        }

        private bool CheckArguments()
        {
            if (string.IsNullOrEmpty(this.OldValue))
            {
                this.Log.LogError("ReplaceTextInFileTask: OldValue is empty string.");
                return false;
            }

            return true;
        }

        private void InitEvaluators(Dictionary<string, Func<string, string>> evaluatorsDictionary)
        {
            if(evaluatorsDictionary == null)
            {
                throw new ArgumentNullException(nameof(evaluatorsDictionary));
            }

            evaluatorsDictionary.Add("{DATE}", _ => DateTime.Today.ToShortDateString());
            evaluatorsDictionary.Add("{UTCDATE}", _ => DateTime.UtcNow.Date.ToShortDateString());
            evaluatorsDictionary.Add("{DATETIME}", _ => DateTime.Now.ToShortDateString());
            evaluatorsDictionary.Add("{UTCDATETIME}", _ => DateTime.UtcNow.ToShortDateString());

            evaluatorsDictionary.Add("{COPYRIGHT}", _ => (DateTime.UtcNow.Year == CurrentYear)? string.Format("© {0}", CurrentYear) : string.Format("© {0} - {1}", CurrentYear, DateTime.UtcNow.Year));
        }
    }
}
