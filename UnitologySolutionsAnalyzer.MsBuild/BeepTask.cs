﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Build.Utilities;
using System.Security;

namespace UnitologySolutionsAnalyzer.MsBuild
{
    /// <summary>
    /// A task to play the sound of a beep through the console speaker.
    /// </summary>
    /// <remarks>
    /// Testing task.
    /// From https://github.com/loresoft/msbuildtasks .
    /// </remarks>
    public class BeepTask : Task
    {
        private int frequency = 800;
        private int duration = 200;

        /// <summary>
        /// Gets or sets the frequency of the beep, ranging from 37 to 32767 hertz.
        /// Defaults to 800 hertz.
        /// </summary>
        /// <value>
        /// The frequency of the beep.
        /// </value>
        public int Frequency
        {
            get
            {
                return this.frequency;
            }
            set
            {
                this.frequency = value;
            }
        }

        /// <summary>
        /// Gets or sets the of the beep measured in milliseconds.
        /// Defaults to 200 milliseconds.
        /// </summary>
        /// <value>
        /// The beep measured in milliseconds.
        /// </value>
        public int Duration
        {
            get
            {
                return this.duration;
            }
            set
            {
                this.duration = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BeepTask"/> class.
        /// </summary>
        public BeepTask()
        {

        }

        /// <summary>
		/// Plays the sound of a beep 
		/// at the given <see cref="Frequency"/> and for the given <see cref="Duration"/> 
		/// through the console speaker.
		/// </summary>
		/// <returns>
		/// Always returns <see langword="true"/>, even when the sound could not be played.
		/// </returns>
        public override bool Execute()
        {
            try
            {
                Console.Beep(this.Frequency, this.Duration);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                this.Log.LogError("BeepTask Error: {0}", ex);
            }
            catch (HostProtectionException ex)
            {
                this.Log.LogError("BeepTask Error: {0}", ex);
            }

            return true;
        }
    }
}
