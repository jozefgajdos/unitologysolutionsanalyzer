#Unitology Solutions Analyzer #

      
An analyzer library for C#  that uses [Roslyn](https://github.com/dotnet/roslyn) to produce refactorings, code analysis, and other niceties.

Establishes rules and code refactoring the code to fix and increase its readability and unification of style within the team.

Inspiration [StyleCop.Analyzers](https://github.com/DotNetAnalyzers/StyleCopAnalyzers/tree/master/StyleCop.Analyzers/StyleCop.Analyzers).

Clone
```shell
git clone https://jozefgajdos@bitbucket.org/jozefgajdos/unitologysolutionsanalyzer.git

```
and build *UnitologySolutionsAnalyzer.proj*

###  Featured

 - Class must have explicit constructor.
 - Regex Match chceck regex string.

#### Nuget
Nuget package is not yet available on [nuget.org](https://www.nuget.org/).
```powershell
Install-Package UnitologySolutionsAnalyzer
```
### Planed fetured

 - Required "this" keyword.
 - Naming - camel case wit first uppercase letter for class, methods and properties.
 - Naming - camel case wit first lowercase letter for private fields and vars.
 - Disabling joining strings using the *+*.
 - Refactor var to explicit type.
 - Static members call only wit class name.
 - Method call parameters must by all in one line or all in separate line.
 - Properties, events and fields must by before methods.
 - Constructor must by first methods.
 - Order method must by public, protected and private.
 - All private fields  must begins lowercase.
 - All public and protected members must begin uppercase.

### Prerequisites
It requires Visual Studio 2015 and .Net Framework 4.6

### License
Copyright (c) 2015 Jozef Gajdoš

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.